<?php
  /**
   * Created by PhpStorm.
   * User: kevin
   * Date: 14/12/2018
   * Time: 09:58
   */
  
  namespace Drupal\flex_bootstrap_layout\Plugin\views\style;
  
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\views\Plugin\views\style\StylePluginBase;
  
  /**
   * Style plugin for the cards view.
   *
   * @ViewsStyle(
   *   id = "flex_style",
   *   title = @Translation("FlexStyle"),
   *   help = @Translation("Displays content  with flexbox properties."),
   *   theme = "views_flex_style",
   *   display_types = {"normal"}
   * )
   */
  class Flex extends StylePluginBase {
    
    /**
     * Specifies if the plugin uses row plugins.
     *
     * @var bool
     */
    protected $usesRowPlugin = TRUE;
    
    /**
     * Does the Style plugin support grouping of rows?
     *
     * @var bool
     */
    protected $usesGrouping = FALSE;
    
    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state) {
      parent::buildOptionsForm($form, $form_state);
      $options                 = [
        ''                        => '-none-',
        'flex-horizontal-start'   => 'start',
        'flex-horizontal-end'     => 'end',
        'flex-horizontal-center'  => 'center',
        'flex-horizontal-between' => 'between',
        'flex-horizontal-around'  => 'around',
        'flex-horizontal-evenly'  => 'evenly',
      ];
      $form['flex_horizontal'] = [
        '#title'         => $this->t('Horizontal alignement'),
        '#description'   => $this->t('Select the horizontal alignement, corresponding to the justify-content property.'),
        '#type'          => 'select',
        '#default_value' => $this->options['flex_horizontal'],
        '#options'       => $options,
      ];
      
      $options               = [
        ''                       => '-none-',
        'flex-vertical-start'    => 'start',
        'flex-vertical-end'      => 'end',
        'flex-vertical-center'   => 'center',
        'flex-vertical-stretch'  => 'stretch',
        'flex-vertical-baseline' => 'baseline',
      ];
      $form['flex_vertical'] = [
        '#title'         => $this->t('Vertical alignement'),
        '#description'   => $this->t('Select the vertical alignement, corresponding to the align-items property.'),
        '#type'          => 'select',
        '#default_value' => $this->options['flex_vertical'],
        '#options'       => $options,
      ];
      
      $options             = [
        ''                   => '-none-',
        'flex-group-start'   => 'start',
        'flex-group-end'     => 'end',
        'flex-group-center'  => 'center',
        'flex-group-stretch' => 'stretch',
        'flex-group-between' => 'between',
        'flex-group-around'  => 'around',
      ];
      $form['flex_group']  = [
        '#title'         => $this->t('Group alignement'),
        '#description'   => $this->t('Select the group alignement, corresponding to the align-content property.'),
        '#type'          => 'select',
        '#default_value' => $this->options['flex_group'],
        '#options'       => $options,
      ];
      $form['child_class'] = [
        '#title'         => $this->t('Child class'),
        '#description'   => $this->t('Add class to the views element.'),
        '#type'          => 'textfield',
        '#default_value' => $this->options['child_class'],
      ];
    }
    
    /**
     * {@inheritdoc}
     */
    protected function defineOptions() {
      $options                    = parent::defineOptions();
      $options['flex_horizontal'] = ['default' => ''];
      $options['flex_vertical']   = ['default' => ''];
      $options['flex_group']      = ['default' => ''];
      $options['child_class']     = ['default' => ''];
      return $options;
    }
    
  }